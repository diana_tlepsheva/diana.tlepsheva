// The program is designed for reading log files and their further processing.

#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <sstream>
#include <filesystem>


//The "LineInfo" class reflects the structure of a single record and the actions performed on it
class LineInfo
{
    public:
        LineInfo(const std::string &code, const std::string &date, const std::string &time, 
		const std::string &priority, const std::string &flag, const std::string &message): 
		m_code(code), m_date(date), m_time(time), m_priority(priority), m_flag(flag), m_message(message) {}
		
        // Getting the period of entries in the log file
        std::string getPeriod();
        // Getting the value of the "priority" field
        std::string getPriority();
	
    private:
        std::string m_code;
        std::string m_date;
        std::string m_time;
        std::string m_priority;
        std::string m_flag;
        std::string m_message;
};

std::string LineInfo:: getPeriod()
{
    return m_date + ' ' + m_time;
}

std::string LineInfo:: getPriority()
{
    return m_priority;
}


/* 
* std::string findPriority(const std::multimap<std::string, LineInfo> &prioritySortMap);
* DESCRIPTION 
*    Determining the number of records for each of the "priorities"
* PARAMETERS 
*    const multimap<string, LineInfo> &prioritySortMap - container for storing events by "priorities" 
*/ 
std::string findPriority(const std::multimap<std::string, LineInfo> &prioritySortMap);

/* 
* std::string findPeriod(std::vector<LineInfo> &arrEvent);
* DESCRIPTION 
*    Getting and displaying the "period" value
* PARAMETERS 
*    vector<LineInfo> &arrEvent - container for storing objects of the "LineInfo" class
*/ 
std::string findPeriod(std::vector<LineInfo> &arrEvent);

/* 
* void split(const std::string &st, std::vector<std::string> &out);
* DESCRIPTION 
*    Splitting a string into parts by the "delim" symbol
* PARAMETERS 
*    const string &st - shared string
*    vector<string> &out - the returned vector
*/
void split(const std::string &st, std::vector<std::string> &out);

/* 
* int getFileSize(std::ifstream &file);
* DESCRIPTION 
*    Getting and displaying the file size
* PARAMETERS 
*    ifstream &file - readable file
* RETURN VALUE 
*    int - file size
*/
int getFileSize(std::ifstream &file);

/* 
* void readFromFile (std::ifstream &file, std::vector<std::string> &out, bool &find_delit);
* DESCRIPTION 
*    Reading data from a file and highlighting records that start with "~#"
* PARAMETERS 
*    ifstream &file - file to read
* 	 vector<string> &out - the returned vector
*    bool &find_delimiter - a variable that takes the value true if at least one character is found "~#"
*/
void readFromFile (std::ifstream &file, std::vector<std::string> &out, bool &find_delimiter);

/* 
* void readingAndProcessingDataInFile(std::string &file_name);
* DESCRIPTION 
*    Reading and processing data in a file.
* PARAMETERS 
*    std::string &file_name - name of the file to be processed
*    std::string &inform - the returned information
*/
void readingAndProcessingDataInFile(std::string &file_name, std::string &inform);


std::string findPriority(const std::multimap<std::string, LineInfo> &prioritySortMap)
{
    static const std::vector<std::string> priority = {"TRACE", "INFO", "DEBUG", "WARN", "ERROR"};
    std::string out = "";
    for (auto& currentPriority: priority)
    {
        out += "Priority \"" + currentPriority + "\" = " + std::to_string(prioritySortMap.count(currentPriority)) + "\n";
    }
    return out;
}

std::string findPeriod(std::vector<LineInfo> &arrEvent)
{
    return "Period: " + arrEvent[0].getPeriod() + " - " +  arrEvent[arrEvent.size() - 1].getPeriod() + "\n";
}

void split(const std::string &st, std::vector<std::string> &out)
{
    std::stringstream f;
    f << st;
    std::string s;
    while (std::getline(f, s, ';')) 
    {
        if (s[0] == ' ')
		{
		    s.erase(0, 1);
		}
        out.push_back(s);
    }
}

int getFileSize(std::ifstream &file)
{
    int size = 0;
    file.seekg(0, std::ios::end);
    size = file.tellg();
	
    return size;
}

void readFromFile (std::ifstream &file, std::vector<std::string> &out, bool &find_delimiter)
{
    std::string line;
    std::string st = "";
    static const std::string delimiter = "~#";

    file.seekg(0, std::ios::beg);
	
    while (getline(file, line))
    {
        if (line.find(delimiter) != std::string::npos)
        {
            if ((st.find(delimiter)) != std::string::npos)
            {
                st += line.substr(0, delimiter.size());
                out.push_back(st);
                find_delimiter = true;
            }
            st = line.substr(line.find(delimiter));
        }
        else
            st += line + '\n';
    }
	
    out.push_back(st);
}

void readingAndProcessingDataInFile(std::string &file_name, std::string &inform)
{
    std::ifstream file(file_name);
    inform = file_name + "\n";

    if (!file.is_open()) 
    {
        inform += "The file cannot be opened!\n";
        return; 
    }
    
    std::vector<std::string> buf;
    bool find_delimiter = false;

   
    inform += "\nFile size: " + std::to_string(getFileSize(file)) + " bytes";
    readFromFile(file, buf, find_delimiter);
    file.close();
        
    if (find_delimiter)
    {
            std::vector<LineInfo> arrEvent;
            std::multimap<std::string, LineInfo> prioritySortMap;
            std::string priority;
            std::vector<std::string> tmp;
            for (int i=0; i<buf.size(); i++)
            {
                tmp.clear();
                split(buf.at(i), tmp);
                LineInfo nEvent(tmp[0], tmp[1], tmp[2], tmp[3], tmp[4], tmp[5]);
                priority = nEvent.getPriority();
                prioritySortMap.insert(std::pair<std::string, LineInfo>(priority, nEvent));
                arrEvent.push_back(nEvent);
            }

            inform += "\nNumber of events = " + std::to_string(arrEvent.size()) + "\n\n" + findPeriod(arrEvent) 
                        + findPriority(prioritySortMap) + "\n";
    }
    else
        inform += "\nThe file does not contain entries that start with ~#.\n\n";
    
    file.close();
}


int main(int argc, char* argv[])
{
    std::string inform;
    std::string file_name;
    std::filesystem::path p(argv[1]);
    for (auto i = std::filesystem::directory_iterator(p); i != std::filesystem::directory_iterator(); i++)
    {
        if (!is_directory(i->path())) 
        {
            inform.clear();
            file_name = argv[1] + '/' + i->path().filename().string();
            readingAndProcessingDataInFile(file_name, inform);
            std::cout << inform << std::endl;
        }
        else
            continue;
    }

    system("pause");
    return 0;
}
